extends ColorRect


func _ready():
	get_parent().get_child(1).connect("snake_died", self, "_on_snake_death")

func _on_snake_death():
	self.set_visible(true)
	Music.stop()
	if get_parent().score > Global.save_data["high_score"]:
		$VBoxContainer/FinalScoreLabel.set_bbcode(" High Score: \n [wave freq=8][rainbow freq=.5 sat=.8 val=.8]" + str(get_parent().score) + "\n")
		Global.save_data["high_score"] = get_parent().score
		Global.save_data()
	else:
		$VBoxContainer/FinalScoreLabel.set_bbcode(" Final Score: \n " + str(get_parent().score) + "\n")
