extends Control

func _ready():
	match Global.save_data["control_option"]:
		"keyboard/controller": $HBoxContainer/VBoxContainer/OptionButton.select(0)
		"touch_screen": $HBoxContainer/VBoxContainer/OptionButton.select(1)
		"onscreen_slider": $HBoxContainer/VBoxContainer/OptionButton.select(2)
	$HBoxContainer/VBoxContainer/SliderSensitivity.set_value(Global.save_data["sensitivity"])
	$HBoxContainer/VBoxContainer2/SliderMusic.set_value(Global.save_data["music_volume"])
	$HBoxContainer/VBoxContainer2/SliderSoundFX.set_value(Global.save_data["sound_fx_volume"])

func _on_OptionButton_item_selected(index):
	SoundButton.play()
	match index:
		0: Global.save_data["control_option"] = "keyboard/controller"
		1: Global.save_data["control_option"] = "touch_screen"
		2: Global.save_data["control_option"] = "onscreen_slider"
	Global.save_data()

func _on_ButtonBack_pressed():
	SoundButton.play()
	self.get_child(3).exit_scene_with_transition("res://MainMenu.tscn")

func _on_SliderSensitivity_value_changed(value):
	Global.save_data["sensitivity"] = $HBoxContainer/VBoxContainer/SliderSensitivity.get_value()
	Global.save_data()
	$HBoxContainer/VBoxContainer/LabelSensitivity.set_text(" Sensitivity:   " + String(Global.save_data["sensitivity"] * 100) + "%")


func _on_SliderMusic_value_changed(value):
	Global.save_data["music_volume"] = $HBoxContainer/VBoxContainer2/SliderMusic.get_value()
	Global.save_data()
	Music.set_volume_db(Global.save_data["music_volume"] * 40 - 38)
	$HBoxContainer/VBoxContainer2/LabelMusic.set_text(" Music Volume:   " + String(Global.save_data["music_volume"] * 100) + "%")


func _on_SliderSoundFX_value_changed(value):
	Global.save_data["sound_fx_volume"] = $HBoxContainer/VBoxContainer2/SliderSoundFX.get_value()
	Global.save_data()
	SoundButton.set_volume_db(Global.save_data["sound_fx_volume"] * 40 - 38)
	$HBoxContainer/VBoxContainer2/LabelSoundFX.set_text(" Sound FX Volume:   " + String(Global.save_data["sound_fx_volume"] * 100) + "%")
