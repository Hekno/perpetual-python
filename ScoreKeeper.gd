extends RichTextLabel

var start_high_score: int
var announced_high_score := false
var animating_till := 0

func _ready():
	get_parent().get_child(1).connect("snake_died", self, "_on_snake_death")
	start_high_score = Global.save_data["high_score"]

func _physics_process(_delta):
	if not get_parent().score < animating_till:
		if get_parent().score > start_high_score and not announced_high_score:
			set_bbcode(" [rainbow freq=.5 sat=.8 val=.8][shake rate=10 level=10]" + str(get_parent().score) + "[/shake][/rainbow] <- New High Score!")
			animating_till = get_parent().score + 500
			SoundHighScore.play()
			announced_high_score = true
		else:
			set_bbcode(" " + str(get_parent().score))

func _on_snake_death():
	self.set_visible(false)
