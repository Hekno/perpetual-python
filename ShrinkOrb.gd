extends Sprite

onready var is_dark := false
var spin_speed: float

func _ready():
	self.set_rotation_degrees(rand_range(0, 360))
	
	if is_dark:
		spin_speed = 400.0
	else:
		spin_speed = 800.0

func spin(delta):
		self.rotate(deg2rad(-spin_speed) * delta)

func update_color():
	if is_dark:
		self.set_modulate(Color.indigo)
	else:
		self.set_modulate(Color.aquamarine)
