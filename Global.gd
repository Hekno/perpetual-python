extends Node

var save_file_path := "user://save.dat"
var save_data = {
		"high_score" : 0,
		"control_option" : "keyboard/controller",
		"sensitivity" : 0.75,
		"music_volume" : 0.50,
		"sound_fx_volume" : 0.50,
	}

func _ready():
	verify_save()

func verify_save():
	var save_file = File.new()
	save_file.open(save_file_path, File.READ)
	if save_file.file_exists(save_file_path):
		load_data()
	elif !save_file.file_exists(save_file_path):
		save_data()
	
func save_data():
	var save_file = File.new()
	save_file.open(save_file_path, File.WRITE)
	save_file.store_string(var2str(save_data))
	save_file.close()

func load_data():
	var save_file = File.new()
	save_file.open(save_file_path, File.READ)
	save_data = str2var(save_file.get_as_text())
	save_file.close()
