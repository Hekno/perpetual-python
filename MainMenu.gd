extends Control

func _ready():
	if Global.save_data["high_score"] != 0:
		$VBoxContainer/HighScoreLabel.set_bbcode("High Score: \n[rainbow freq=.3 sat=.8 val=.8]" + str(Global.save_data["high_score"]))
	Music.stop()

func _on_ButtonPlay_pressed():
	SoundButton.play()
	self.get_child(3).exit_scene_with_transition("res://Arena.tscn")


func _on_ButtonSettings_pressed():
	SoundButton.play()
	self.get_child(3).exit_scene_with_transition("res://SettingsMenu.tscn")


func _on_ButtonAbout_pressed():
	SoundButton.play()
	self.get_child(3).exit_scene_with_transition("res://AboutMenu.tscn")
