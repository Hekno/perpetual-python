extends Control

func _on_ButtonBack_pressed():
	SoundButton.play()
	self.get_child(2).exit_scene_with_transition("res://MainMenu.tscn")


func _on_LabelSourceCode_meta_clicked(meta):
	OS.shell_open(meta)


func _on_LabelItchPage_meta_clicked(meta):
	OS.shell_open(meta)


func _on_LabelDescription_meta_clicked(meta):
	OS.shell_open(meta)
