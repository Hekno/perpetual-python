A game made for the Godot Wild Jam #36 https://itch.io/jam/godot-wild-jam-36

The theme was "Uncontrollable Growth"

The code is available under the GPLv3 and the assets are available under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0) license
