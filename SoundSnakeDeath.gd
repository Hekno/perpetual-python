extends AudioStreamPlayer


func _ready():
	get_parent().get_parent().get_child(1).connect("snake_died", self, "_on_snake_death")
	set_volume_db(Global.save_data["sound_fx_volume"] * 40 - 38)

func _on_snake_death():
	play()
