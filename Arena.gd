extends Node2D

onready var score: int
onready var orbs := []
var frame := 0

func _ready():
	Music.play()

func _physics_process(delta):
	if $Snake.alive:
		if frame % 405 == 0:
			_spawn_orb(false)
		elif frame % 135 == 0:
			_spawn_orb(true)
		for orb in orbs:
			orb.spin(delta)
		frame += 1
		score += 5


func _on_ButtonPlayAgain_pressed():
	SoundButton.play()
	self.get_child(4).exit_scene_with_transition("res://Arena.tscn")


func _on_ButtonMainMenu_pressed():
	SoundButton.play()
	self.get_child(4).exit_scene_with_transition("res://MainMenu.tscn")

func _spawn_orb(is_dark):
	var orb := preload("res://ShrinkOrb.tscn").instance()
	orb.is_dark = is_dark
	orb.update_color()
	var rand_orb_coords: Vector2
	var too_close_objects: int
	while true:
		rand_orb_coords = Vector2(rand_range(100, 1180), rand_range(100, 620))
		too_close_objects = 0
		for o in orbs:
			if (o.get_position().x < rand_orb_coords.x + 24
			and o.get_position().x > rand_orb_coords.x - 24
			and o.get_position().y < rand_orb_coords.y + 24
			and o.get_position().y > rand_orb_coords.y - 24):
				too_close_objects += 1
		for segment in $Snake.segments:
			if (segment.get_position().x < rand_orb_coords.x + 24
			and segment.get_position().x > rand_orb_coords.x - 24
			and segment.get_position().y < rand_orb_coords.y + 24
			and segment.get_position().y > rand_orb_coords.y - 24):
				too_close_objects += 1
		if ($Snake.segments[0].get_position().x < rand_orb_coords.x + 250
		and $Snake.segments[0].get_position().x > rand_orb_coords.x - 250
		and $Snake.segments[0].get_position().y < rand_orb_coords.y + 250
		and $Snake.segments[0].get_position().y < rand_orb_coords.y - 250):
			too_close_objects += 1
		if too_close_objects > 0:
			continue
		break
	orb.set_position(rand_orb_coords)

	add_child(orb, true)
	orbs.append(orb)
