extends Sprite

func enter_transition():
	self.get_child(0).play("EnterTransition")
	yield(self.get_child(0), "animation_finished")
	
func exit_scene_with_transition(scene: String):
	self.get_child(0).play("ExitTransition")
	yield(self.get_child(0), "animation_finished")
	
	if get_tree().change_scene(scene) != OK:
		print("Could not change scene")
