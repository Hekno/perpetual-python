extends HSlider

func _ready():
	if Global.save_data["control_option"] != "onscreen_slider":
		self.set_visible(false)
	else:
		self.set_visible(true)
