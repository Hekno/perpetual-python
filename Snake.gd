extends Node2D

var segments := []
var queued_remove_segments := 0

var movement_speed := 512
onready var rotation_speed = 9 * Global.save_data["sensitivity"]
var starting_segments := 30

signal snake_died
var alive := true
var frame = 0

func _ready():
	_spawn_segments(starting_segments)
	
	# make sure head is rendered on top (segments start at -2)
	segments[0].set_z_index(-1)
	segments[0].set_position(Vector2(640, 640))
	segments[0].set_texture(preload("res://Head.svg"))
	segments[0].add_child(preload("res://Tongue.tscn").instance(), true)

func _physics_process(delta):
	if alive:
		var input_rotation: float
		# get rotation amount based on input
		match Global.save_data["control_option"]:
			"keyboard/controller":
				input_rotation = (Input.get_action_strength("right") - Input.get_action_strength("left")) * delta * rotation_speed
			"onscreen_slider":
				input_rotation = $HSlider.value * delta * rotation_speed
			"touch_screen":
				var side = 0
				if Input.is_mouse_button_pressed(1):
					if get_viewport().get_mouse_position().x > get_viewport_rect().size.x / 2:
						side += 1
					else:
						side -= 1
				input_rotation = side * delta * rotation_speed
		# rotate head with that input
		segments[0].rotate(input_rotation)
		# move the head based on current rotation
		segments[0].translate(Vector2.UP.rotated(segments[0].get_rotation()) * delta * movement_speed)
		# iterate from last item to first in segments to get the transform and rotation from the item 1 index lower
		for i in range(len(segments) - 1, 0, -1):
			segments[i].set_rotation(segments[i - 1].get_rotation())
			segments[i].set_transform(segments[i - 1].get_transform())
		_remove_segments()
		frame += 1
		if frame % 3 == 0:
			_spawn_segments(1)
		_check_if_collided()

func _spawn_segments(count):
	for i in range(abs(count)):
		var segment = preload("res://Segment.tscn").instance()
		#spawn off screen so they don't show up in the top left corner
		segment.set_position(Vector2(-64, -64))
		add_child(segment, true)
		segments.append(segment)

func _remove_segments():
	if queued_remove_segments > 0 and len(segments) > 1:
		segments.pop_back().queue_free()
		queued_remove_segments -= 1

func _queue_remove_segments(count):
	queued_remove_segments += count

func _check_if_collided():
	var orbs = get_parent().orbs
	for i in range(0, len(orbs)):
		if segments[0].get_position().distance_squared_to(orbs[i].get_position()) < 1500:
			SoundOrb.play()
			if orbs[i].is_dark:
				_queue_remove_segments(3 * (len(segments) - queued_remove_segments) / 16)
			else:
				_queue_remove_segments(3 * (len(segments) - queued_remove_segments) / 8)
			orbs[i].queue_free()
			orbs.remove(i)
			segments[0].get_child(0).get_child(0).play("FlickTongue")
			break
			
	for i in range(len(segments)):
		var distance = segments[0].get_position().distance_squared_to(segments[i].get_position())
		if (distance < 2500 && i > starting_segments 
		or segments[0].position.x > 1254.5
		or segments[0].position.x < 25.5
		or segments[0].position.y > 694.5
		or segments[0].position.y < 25.5):
			emit_signal("snake_died")
			alive = false
			segments[0].set_texture(preload("res://HeadDead.svg"))
			segments[0].get_child(0).set_visible(true)
			break
		
